<?php

class Core {
    public static $dbhost;
    public static $dbuser;
    public static $dbpass;
    public static $dbname;
    public static $db;
    
    public static function init($host, $user, $pass, $name){
        try {
            self::$db = new mysqli($host, $user, $pass, $name);
            
            if(self::$db->connect_error){
                throw new Exception('Could not connect to database');
            }
        } catch (Exception $ex) {
            die('Error message: ' . $ex->getMessage());
        }
    }
}
